package com.tsc.jarinchekhina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.tsc.jarinchekhina.tm.command.AbstractDataCommand;
import com.tsc.jarinchekhina.tm.dto.Domain;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class BackupLoadCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "backup-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load backup from file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final File file = new File(FILE_BACKUP);
        if (!file.exists()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_BACKUP)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @Nullable final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

}
