package com.tsc.jarinchekhina.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public class Backup extends Thread {

    @NotNull
    private static final String CMD_SAVE = "backup-save";

    @NotNull
    private static final String CMD_LOAD = "backup-load";

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(bootstrap.getPropertyService().getBackupInterval());
        }
    }

    @SneakyThrows
    public void init() {
        load();
        start();
    }

    @SneakyThrows
    public void save() {
        bootstrap.parseCommand(CMD_SAVE);
    }

    @SneakyThrows
    public void load() {
        bootstrap.parseCommand(CMD_LOAD);
    }

}
